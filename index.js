const express = require('express');
const path = require('path');
const mysql = require('mysql');
const bodyParser = require('body-parser');   /* Sert pour les POST */
const nodemailer = require('nodemailer');

const app = express();

const connection = mysql.createConnection({
    host     : "localhost",
    user     : "root",
    password : "root",
    database : "bieres_bzh"
});

connection.connect(function(error) {
        if (error) {
            console.error('Connection error.', error);
            return;
        }
        console.log('Established connection.', connection.threadId);
    });

connection.on('close', function(err) {
  if (err) {
    // Reconnection en cas d'erreur
    connection = mysql.createConnection(connection.config);
  } else {
    console.log('Connection closed normally.');
  }
});

app.set('view engine', 'ejs');

app.use(express.static(path.join(__dirname, 'public')));
app.use(bodyParser.urlencoded({ extended: true })); //pour recupérer les donneés passées en post

app.get('/',function(req,res){
    res.render('accueil.ejs');
});


app.get('/brasseries',function(req,res){
    connection.query('SELECT * FROM Brasseries ORDER BY nom', function(err, rows, fields){
        res.render('brasseries.ejs',{
            items : rows
        });
    });
});


app.get('/fiche_brasserie/:brasserie',function(req,res){
    var brasserie = req.params.brasserie;
    connection.query('SELECT * , Brasseries.nom AS nomBrass, Bieres.nom AS nomBiere FROM Brasseries JOIN Bieres ON Brasseries.id = Bieres.brasserie WHERE Brasseries.nom = ? ORDER BY nomBiere', [brasserie], function(err, rows, fields){
        console.log(rows.length);
        if(rows.length > 0){
            res.render('fiche_brasserie.ejs',{
            items : rows
        });
        }else{
            console.log(brasserie);
            connection.query('SELECT * FROM Brasseries WHERE nom = ?',[brasserie],function(err,rows,fields){
                res.render('fiche_brasserie_incomplete.ejs',{
                    items : rows
                });
            });
        }
    });
});

app.get('/fiche_biere/:biere',function(req,res){
    var biere = req.params.biere;
    connection.query('SELECT * , Brasseries.nom AS nomBrass, Bieres.nom AS nomBiere FROM Bieres JOIN Brasseries ON Bieres.brasserie = Brasseries.id WHERE Bieres.nom = ?', [biere], function(err,rows,fields){
        res.render('fiche_biere.ejs',{
           // verif si erreur
            item: rows[0]
        });
    });
});

app.get('/contact',function(req,res){
    res.render('contact.ejs');
});

app.post('/contact/sendMail',function(req,res){
    var {nom, 
         mail, 
         msg}
    = req.body;
    console.log(req.body);

    nodemailer.createTestAccount((err, account) => {

        console.log('error nodemailer', err);
        // create reusable transporter object using the default SMTP transport
        let transporter = nodemailer.createTransport({
            host: 'smtp.gmail.com',
            port: 465,
            secure: true, // true for 465, false for other ports
            auth: {
                user: 'bzz.mayalabeille@gmail.com', // generated ethereal user
                pass: '********'  // generated ethereal password
            },
            tls: { rejectUnauthorized: false }
        });

        // setup email data with unicode symbols
        let mailOptions = {
            from: '"Site bières bretonnes " <foo@blurdybloop.com>', // sender address
            to: 'morgane.legarlantezec@gmail.com', // list of receivers
            subject: 'Demande bière', // Subject line
            text: 'sfsdf', // plain text body
            html: '<p>test envoi de mail</p>' // html body
        };

        // send mail with defined transport object
        transporter.sendMail(mailOptions, (error, info) => {
            if (error) {
                return console.log('transport error',error);
            }else{
                res.render('mail_ok.ejs',{personne : nom});          
            }
        });
    });
});

app.get('/admin',function(req,res){
    res.render('admin.ejs');
});

app.post('/admin/insert_brass',function(req,res){
    var {brassNom,     // string length<100
         brassAdresse, // string length<100
         brassVille,   // string length<100
         brassCode,    // int length<5
         brassLogo,    // string length<100
         brassSite}    // string length<100
    = req.body;
    //tableau pr les verifs :
    /* var champsBrass [
        {
            colonne:'brassNom',
            type:'string',
            longueurMax:100
        },
        {
            colonne:'brassAdresse',
            type:'string',
            longueurMax:100
        },
        {
            colonne:'brassVille',
            type:'string',
            longueurMax:100
        },
        {
            colonne:'brassCode',
            type:'number',
            longueurMax:5
        },
        {
            colonne:'brassLogo',
            type:'string',
            longueurMax:100
        },
        {
            colonne:'brassSite',
            type:'string',
            longueurMax:100
        },
    ]
    */


    console.log("nomBrass: " + brassNom);
    console.log("adresse: "  + brassAdresse);
    console.log("ville: "    + brassVille);
    console.log("code: "     + brassCode);
    console.log("logo: "     + brassLogo);
    console.log("site: "     + brassSite);
    if(brassNom !=undefined && brassAdresse !=undefined && brassVille !=undefined && brassCode !=undefined && brassLogo !=undefined && brassSite !=undefined){
        if(brassNom.length < 1)     {brassNom = 'null';}
        if(brassAdresse.length < 1) {brassAdresse = 'null';}
        if(brassVille.length < 1)   {brassVille = 'null';}
        if(brassCode.length < 1)    {brassCode = 'null';}
        if(brassLogo.length <1)     {brassLogo = 'null';}
        if(brassSite.length < 1)    {brassSite = 'null';}
        connection.query('INSERT INTO Brasseries(nom,adresse,ville,code_postal,logo,site) VALUES(?,?,?,?,?,?)',[brassNom,brassAdresse,brassVille,brassCode,brassLogo,brassSite], function(err,rows,fields){
            //  if(!err) message OK
            res.render('admin.ejs',{
                brassSuccess : 'Y'
            });
        });
    }else{
        res.render('admin.ejs',{
           brassSuccess : 'N'
        });
    }
});

app.post('/admin/insert_biere',function(req,res){
    var {biereNom,    // string length<100
         biereDegres, // number length<5
         biereType,   // string length<50
         biereBrass,  // number
         biereDesc,   // string
         bierePhoto}  // string length<100
    = req.body;
    console.log("nomBiere: "  + biereNom);
    console.log("degres: "    + biereDegres);
    console.log("type: "      + biereType);
    console.log("brasserie: " + biereBrass);
    console.log("descr: "     + biereDesc);
    console.log("photo: "     + bierePhoto);
    if(biereNom !=undefined && biereDegres !=undefined && biereType !=undefined && biereBrass !=undefined && biereDesc !=undefined && bierePhoto !=undefined){
        if(biereNom.length < 1)    {biereNom = 'null';}
        if(biereDegres.length < 1) {biereDegres = 'null';}
        if(biereType.length < 1)   {biereType = 'null';}
        if(biereBrass.length < 1)  {biereBrass = 'null';}
        if(biereDesc.length < 1)   {biereDesc = 'null';}
        if(bierePhoto.length < 1)  {bierePhoto = 'null';}
        connection.query('INSERT INTO Bieres(nom,degres,famille,brasserie,description,photo) VALUES(?,?,?,?,?,?)',[biereNom,biereDegres,biereType,biereBrass,biereDesc,bierePhoto],function(err,rows,fields){
            res.render('admin.ejs',{
                biereSuccess : 'Y'
            });
        });
    }else{
        res.render('admin.ejs',{
            brassSuccess : 'N'
        });
    }
});

/* Recherche par nomBrasserie, nomBiere, ville, type, description */
//var urlencodedParser = bodyParser.urlencoded({ extended: false });
app.post('/recherche',/*urlencodedParser,*/function(req,res){
    var searched  = req.body.search
    ,   rech      = '%' + req.body.search + '%'
    ,   resBrass // a supprimer
    ,   resBiere // a supprimer
    ,   resVille // a supprimer
    ,   resType // a supprimer
    ,   resDescr; // a supprimer

    var nbRequest = 5;

var sendResults = function(){
        if(nbRequest == 0) {
        console.log("sendresult");
           if(resBrass !=undefined || resBiere !=undefined || resVille !=undefined || resType !=undefined || resDescr !=undefined){
                res.render('recherche.ejs',{
                    resBrass,
                    resBiere,
                    resVille,
                    resType,
                    resDescr,
                    searched
                });
            }else{
                res.render('recherche_noResult.ejs',{
                    searched
                });
            }
         }
    };

    connection.query('SELECT nom FROM Brasseries WHERE nom LIKE ?', [rech], function(err,rows,fields){
        if(rows.length > 0){
           resBrass = rows;
        }
        nbRequest --;
        sendResults();
    });

    connection.query('SELECT nom FROM Bieres WHERE nom LIKE ?', [rech], function(err,rows,fields){
        if(rows.length > 0){
           resBiere = rows;
        }
        nbRequest --;
        sendResults();
    });

    connection.query('SELECT nom, ville FROM Brasseries WHERE ville LIKE ?', [rech], function(err,rows,fields){
        if(rows.length > 0){
            resVille = rows;
        }
        nbRequest --;
        sendResults();
    });

    connection.query('SELECT nom, famille FROM Bieres WHERE famille LIKE ?', [rech], function(err,rows,fields){
        if(rows.length > 0){
            resType = rows;
        }
        nbRequest --;
        sendResults();
    });

    connection.query('SELECT nom, description FROM Bieres WHERE description LIKE ?', [rech], function(err,rows,fields){
        if(rows.length > 0){
            resDescr = rows;
        }
        nbRequest --;
        sendResults();
    });

});

app.listen(8080);
