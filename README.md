# SITE BIERES BRETONNES

Site d'information répertoriant un maximum de bières bretonnes, classées par brasseries. Projet perso commencé lors de ma formation en Développement web au [Callac Soft College](http://www.callac-soft-college.fr/) et poursuivi ensuite.

Merci à [Gael Monbrault](http://gaelweb.fr/) pour son aide a faire les maquettes.

## Description du site 

### Page "Brasseries"

Les brasseries sont présentées sous forme de vignettes avec logo, nom et ville de la brasserie ([exemple](http://www.tykemm.bzh/fabricants)).
Au clic sur une des vignettes, on accède à une nouvelle page (la fiche de la brasserie) avec les informations suivantes :


- nom de la brasserie
- ville
- bieres produites
- logo
- lien vers site internet

Au clic sur une des bières produites, on accède à une nouvelle page (la fiche de la bière) avec les informations suivantes :

- Nom de la bière
- brasserie
- photo 
- description
- carte

### Page "Contact"

Formulaire qui permet au visteur de contacter l'administrateur du site.

### Page d'administration

Sécurisée par un mot de passe

- ajout d'une brasserie
- modification des fiches existantes
- update des données
- mise à jour de la page coup de coeur

### Système de recherche

Barre de recherche pour l'utilisateur en haut à droite. Recherche par brasserie/nom de bière/type de bière/ville.

La recherche s'effectue dans les noms de bières, de types de bières, de brasseries ou de villes. 

- Si un résultat est trouvé : une page avec la liste des resultats s'affiche. Ce sont des liens vers les pages correspondantes
- Si aucun résultat n'est trouvé : une page avec un message "aucun résultat trouvé" s'affiche


## Technologies utilisées
- HTML
- CSS
- JavaScript
- MySQL
- node.js

## Build Setup

Fonctionne avec node v.6.9.5

#### install dependencies
npm install

#### serve with hot reload at localhost:8080 (pour lancer application)
npm run dev

#### Ouvrir un second terminal et lancer le serveur :
npm run server

#### build for production with minification
npm run build

#### build for production and view the bundle analyzer report
npm run build --report


