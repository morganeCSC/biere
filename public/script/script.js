(function(){
    var main_nav = document.getElementById('main_nav');
    main_nav.classList.add('invisible');

    var btn = document.getElementById('burger_btn');
    btn.onclick = function(){
        if(!main_nav.classList.contains('menu_unfolded')){
            main_nav.classList.remove('invisible');
            main_nav.classList.add('menu_unfolded');
            setTimeout(closeMenu,10);
        }
    };

function closeMenu(){
           document.onclick = function(e){
        var inmenu = false;
        var t = e.target;
        while(t){
            if(t == main_nav){
                inmenu = true;
                break;
            }
            t = t.parentElement;
        }
        if(!inmenu){
            main_nav.classList.remove('menu_unfolded');
            main_nav.classList.add('invisible');
            document.onclick = null;
        }
    }
}

})();
